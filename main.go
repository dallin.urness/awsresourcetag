package main

import (
	"context"
	"encoding/csv"
	"fmt"
	"log"
	"os"

	"github.com/aws/aws-sdk-go-v2/config"
	tagapi "github.com/aws/aws-sdk-go-v2/service/resourcegroupstaggingapi"
)

func readCsvFile(filePath string) [][]string {
	f, err := os.Open(filePath)
	if err != nil {
		log.Fatal("Unable to read input file "+filePath, err)
	}
	defer f.Close()

	csvReader := csv.NewReader(f)
	records, err := csvReader.ReadAll()
	if err != nil {
		log.Fatal("Unable to parse file as CSV for "+filePath, err)
	}

	return records
}

func getResourceChangesFromCsv(files ...string) []ResourceChange {
	const (
		NAME_IND        = 0
		ACCOUNT_IND     = 2
		SERVICE_TAG_IND = 3
		TEAM_TAG_IND    = 4
		TO_TAG_IND      = 5
		ARN_IND         = 8
		YES_VALUE       = "Y"
	)

	resourceChanges := make([]ResourceChange, 0)

	for _, file := range files {
		parsedCsv := readCsvFile(file)
		for i := 1; i < len(parsedCsv); i++ {
			if parsedCsv[i][TO_TAG_IND] == YES_VALUE {
				name := parsedCsv[i][NAME_IND]
				arn := parsedCsv[i][ARN_IND]
				serviceTag := parsedCsv[i][SERVICE_TAG_IND]
				teamTag := parsedCsv[i][TEAM_TAG_IND]
				account := parsedCsv[i][ACCOUNT_IND]
				resourceChanges = append(resourceChanges, ResourceChange{Name: name, Account: account, ServiceTag: serviceTag, TeamTag: teamTag, Arn: arn})
			}
		}
	}

	return resourceChanges
}

type ResourceChange struct {
	Name       string
	Account    string
	ServiceTag string
	TeamTag    string
	Arn        string
}

func main() {

	accountArg := os.Args[1]

	resourceChanges := getResourceChangesFromCsv("Orphaned Resources - 2023_01_06_Legacy.csv") // "Orphaned Resources - 2023_01_03 Org Accounts.csv",

	// group by account
	resourceChangesByAccount := make(map[string][]ResourceChange, 0)
	for _, resourceChange := range resourceChanges {
		if val, ok := resourceChangesByAccount[resourceChange.Account]; ok {
			resourceChangesByAccount[resourceChange.Account] = append(val, resourceChange)
		} else {
			resourceChangesByAccount[resourceChange.Account] = make([]ResourceChange, 0)
			resourceChangesByAccount[resourceChange.Account] = append(val, resourceChange)
		}
	}

	// aws session

	ctx := context.Background()

	config, err := config.LoadDefaultConfig(ctx)
	if err != nil {
		log.Fatal("Failed to create aws session, may need to paste credentials into terminal")
	}
	client := tagapi.NewFromConfig(config)

	// get resources to tag
	var toChange []ResourceChange
	if value, ok := resourceChangesByAccount[accountArg]; ok {
		toChange = value
	} else {
		fmt.Println("Environment entered does not exist, or there are no accounts that need to be tagged")
		return
	}

	fmt.Print("Num To Tag: ")
	fmt.Println(len(toChange))

	// print pre-tag
	fmt.Println("\nResources to be tagged: ")
	for _, r := range toChange {
		fmt.Printf("NAME: %v | ARN: %v | Tags: Team -> %v, Service -> %v\n", r.Name, r.Arn, r.TeamTag, r.ServiceTag)
	}
	fmt.Print("\n\n")
	fmt.Println("Do you want to continue with these changes (y/n)?")
	var response string
	fmt.Scanln(&response)

	switch response {
	case "n":
		fmt.Println("Aborted")
		return
	case "y":
	default:
		fmt.Println("Unrecognized response, aborting")
		return
	}

	// tag selected resources
	successCount := 0
	failedCount := 0
	errorList := make([]string, 0)
	for _, c := range toChange {
		input := &tagapi.TagResourcesInput{
			ResourceARNList: []string{c.Arn},
			Tags:            map[string]string{"Team": c.TeamTag, "Service": c.ServiceTag},
		}

		output, err := client.TagResources(ctx, input)
		if err != nil {
			errorList = append(errorList, c.Arn+"  : "+err.Error())
			failedCount++
			break
		} else {
			fmt.Println(output.FailedResourcesMap)
			if len(output.FailedResourcesMap) > 0 {
				failedCount++
			} else {
				successCount++
			}
		}
	}

	fmt.Print("Succcessful: ")
	fmt.Println(successCount)
	fmt.Print("Failed: ")
	fmt.Println(failedCount)
	fmt.Println("Errors: ")

	for _, e := range errorList {
		fmt.Println(e)
	}
}
